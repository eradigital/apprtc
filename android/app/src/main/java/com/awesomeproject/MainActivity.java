package com.awesomeproject;
import android.content.Intent;
import com.facebook.react.BuildConfig;
import com.facebook.react.ReactActivity;
import android.content.Intent;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import android.util.Log;

public class MainActivity extends ReactActivity {
  public  boolean  isOnNewIntent = false;

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "AwesomeProject";
  }

  @Override
  public  void  onNewIntent(Intent  intent) {
      super.onNewIntent(intent);
      isOnNewIntent = true;
      ForegroundEmitter();
  }

  @Override
  protected  void  onStart() {
      super.onStart();
      if(isOnNewIntent == true){}else {
          ForegroundEmitter();
      }
  }

  public  void  ForegroundEmitter(){
      String  main = getIntent().getStringExtra("mainOnPress");
      String  btn = getIntent().getStringExtra("buttonOnPress");
      WritableMap  map = Arguments.createMap();
      if (main != null) {
          map.putString("main", main);
      } 
      if (btn != null) {
          map.putString("button", btn);
      }
      try {
          getReactInstanceManager().getCurrentReactContext()
          .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
          .emit("notificationClickHandle", map);  	
      } catch (Exception  e) {	
      Log.e("SuperLog", "Caught Exception: " + e.getMessage());
      }
  }
}
