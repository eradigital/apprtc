// /* eslint-disable react-native/no-inline-styles */
// import React, {useEffect, useRef, useState} from 'react';
// import {
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
//   PermissionsAndroid,
//   Dimensions,
// } from 'react-native';
// import {
//   RTCPeerConnection,
//   RTCIceCandidate,
//   RTCSessionDescription,
//   RTCView,
//   MediaStream,
//   MediaStreamTrack,
//   mediaDevices,
//   registerGlobals,
// } from 'react-native-webrtc';
// import {requestCameraPermission} from './src/functions/permission.function';
// import io from 'socket.io-client';
// const windowWidth = Dimensions.get('window').width;
// const windowHeight = Dimensions.get('window').height;

// export default function App() {
//   const [streamVideo, setStreamVideo] = useState();

//   const constraints = {
//     video: true,
//     audio: false,
//   };

//   useEffect(() => {
//     if (requestCameraPermission()) {
//       mediaDevices
//         .getDisplayMedia(constraints)
//         .then(stream => {
//           setStreamVideo(stream);
//         })
//         .catch(error => {
//           console.error('Error accessing media devices.', error);
//         });
//     }
//     return () => {};
//   }, []);

//   console.log(streamVideo);

//   return (
//     <View style={styles.wrapper}>
//       <View style={{height: windowHeight / 2, width: windowWidth}}>
//         <RTCView
//           key={1}
//           zOrder={9}
//           objectFit="cover"
//           style={{flex: 1}}
//           streamURL={streamVideo && streamVideo.toURL()}
//           type="local"
//         />
//       </View>
//       <TouchableOpacity
//         style={{
//           backgroundColor: 'green',
//           paddingVertical: 10,
//           paddingHorizontal: 20,
//           borderRadius: 10,
//         }}>
//         <Text style={{color: 'white'}}>Share Screen</Text>
//       </TouchableOpacity>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   wrapper: {
//     flex: 1,
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
// });
import React, {useState, useEffect} from 'react';
import {SafeAreaView, StatusBar, Text, NativeModules} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {RTCView, mediaDevices} from 'react-native-webrtc';
// import ReactNativeForegroundService from '@supersami/rn-foreground-service';

const {ScreenShareFile} = NativeModules;

const App = () => {
  const [stream, setstream] = useState(undefined);

  useEffect(() => {
    // ReactNativeForegroundService.add_task(
    //   () => console.log('I am Being Tested'),
    //   {
    //     delay: 100,
    //     onLoop: true,
    //     taskId: 'taskid',
    //     onError: e => console.log(`Error logging:`, e),
    //   },
    // );

    // ReactNativeForegroundService.start({
    //   id: 144,
    //   title: 'Foreground Service',
    //   message: 'you are online!',
    // });
    return () => {};
  }, []);

  const startShare = async () => {
    mediaDevices
      .getDisplayMedia({video: true})
      .then(handleSuccess, handleError);
  };

  const stopShare = async () => {
    mediaDevices.stopShare();
  };

  const handleError = error => {
    console.log('Error', error);
  };

  const handleSuccess = async stream => {
    setstream(stream);
  };

  const backgroundStyle = {
    backgroundColor: Colors.lighter,
    flex: 1,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={'dark-content'} />

      <Text
        onPress={() => {
          startShare();
        }}
        style={{textAlign: 'center', width: '100%', marginVertical: 20}}>
        Press me to start sharing
      </Text>
      {stream && (
        <RTCView
          style={{
            borderColor: '#000',
            borderWidth: 1,
            width: '80%',
            height: '80%',
          }}
          streamURL={stream?.toURL()}
        />
      )}
      <Text
        onPress={() => {
          stopShare();
        }}
        style={{textAlign: 'center', width: '100%', marginVertical: 20}}>
        Press me to stop sharing
      </Text>
    </SafeAreaView>
  );
};

export default App;
